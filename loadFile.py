# -*- coding: utf-8 -*-
import decimal
import math
import re

import numberCalculations as nC

eqtTypes = {'msxy':'magnetic','esxy':'electrostatic'}
boundaryTypes = {'diri':{'msxy':['magnetic_potential','magnetic_potential_real'],'esxy':['electrostatic_potential','electrostatic_potential']}}
materialTypes = {'msxy':['magnetic_permeability','magnetic_current_density_external_real'],'esxy':['electrostatic_permittivity','electrostatic_charge_density']}

class Edge():
    """Klasa Edge
        Odpowiada krawędziom obiektu w programie Agros 2D
        Jeżeli krawędź jest krzywą posiada wtedy parametry angle,circle.

        Parametr:
            p1 (PointFatObject): Punkt początkowy krawędzi
            p2 (PointFatObject): Punkt końcowy krawędzi
            boundary (string): Nazwa warunku brzegowego.
            angle (int): Kąt krawędzi (min 0 max 90).
            circle (CircleFatObject): Okrąg na którym znajduje się krzywa.
            middle (PointFatObject): Punkt środka krawędzi.
            middleOfCurve (PointFatObject): Punkt środka krzywej.
    """
    def __init__(self,p1,p2,angle=0,circle=None):
        """Inicjalizacje obiektu klasy Edge
            Jeżeli krawędź ma być krzywą należy podać argumenty angle lub circle.
            Jeżeli podany zostanie argument angle wartość kąta jest przypisywana taka jaka została podana w argumencie angle.
            Jeżeli podany został argument circle wartość angle jest wyliczany w taki sposób aby krawędź leżała na okręgu.

        Args:
            p1 (PointFatObject): Punkt początkowy krawędzi.
            p2 (PointFatObject): Punkt końcowy krawędzi.
            angle (int, optional): Kąt krzywej. Defaults to 0. Min 0. Max 90
            circle (CircleFatObject, optional): Okrąg na którym leży krzywa. Defaults to None.
        """
        self.p1,self.p2 = p1,p2
        self.boundary = ''
        self.middle = PointFatObject(X=(self.p1.X+self.p2.X)/2, Y=(self.p1.Y+self.p2.Y)/2)
        self.middleOfCurve = self.middle
        self.angle = angle
        if circle != None:
            self.circle = circle
            self.calculateAngle()

    def __str__(self):
        """Zwraca string klasy

        Returns:
            str: String w formacie "p1: [punkt] || p2: [punkt] ANGLE: [kat]"
        """
        return "p1: " + str(self.p1)+' || p2: '+str(self.p2)+ ' ANGLE:' + str(self.angle)

    def __ne__(self,other):
        return not self.__eq__(other)

    def __eq__(self,other):
        """Zwraca czy 2 krawędzie są sobie równe

            Zwraca True jeżeli obie krawędzie mają ten sam kąt p1 i p2 albo
            jeżeli kąt wynosi 0 to jeżeli p1 == p2 i p2 == p1 dwóch krawędzi

        Returns:
            boolean: Czy krawędzie są sobie równe.
        """
        if self.p1 == other.p1 and self.p2 == other.p2 and self.angle == other.angle:
            return True
        if other.angle == 0 and self.angle == 0 and self.p1 == other.p2 and self.p2 == other.p1:
            return True
        return False

    def __hash__(self):
        return hash( (self.p1.X,self.p1.Y,self.p2.X,self.p2.Y) )

    def getBisection(self):
        """Zwraca parametry równania prostej prostopadłej do krawędzi

        Returns:
            tuple[int, int]: Parametry równania według wzoru Y = tuple[0] * X + tuple[1].
        """
        if self.p1.Y == self.p2.Y:
            return "inf",self.middle.X
        slope = -(self.p1.X-self.p2.X)/(self.p1.Y-self.p2.Y)    
        c = self.middle.Y - self.middle.X*slope
        return slope,c

    def getLine(self):
        """Funkcja zwraca parametry równania prostej na której leży krawędź.
        Weług wzoru Y = slope * X + c

        Returns:
            tuple[float,float]: Parametry slope i c równania prostej opisanej wzorem Y = slope * X + c
        """
        slope = (self.p1.Y-self.p2.Y)/(self.p1.X-self.p2.X)
        c = (self.p1.Y - slope * self.p1.X )
        return slope,c

    def pointOnEdge(self,point):
        """Funkcja zwraca czy punkt znajduje się na krawędzi czy nie.

        Args:
            point (PointFatObject): Punkt który ma być sprawdzony czy leży na krawędzi.

        Returns:
            boolean: True jeżeli punkt leży na krawędzi, False w przeciwnym wypadku.
        """

        if self.p1.X == self.p2.X:
            return (point.X == self.p1.X) and nC.numberBetween(point.Y,self.p1.Y,self.p2.Y)

        slope,c = self.getLine()

        return point.Y == point.X * slope + c and nC.numberBetween(point.X,self.p1.X,self.p2.X)

    def calculateAngle(self):
        """Wylicza kąt na podstawie punktów i okręgu przyspianego do obiektu
            i przypisuje wartości angle do obiektu.

        Returns:
            Edge: Zwraca wywoływany obiekt po wyliczeniu kąta.
        """
        m,c = self.getBisection()
        self.middleOfCurve = self.middle.getClosest( self.circle.getCommonPointsWithLine(m, c) )
        self.angle = (180 - self.middleOfCurve.calculateAngleFromVertex(self.p1, self.p2) )* 2
        self.p1,self.p2 = self.p2,self.p1
        return self

class FatObject(object):
    """Klasa podstawowa odpowiedzialna za interpretowanie obiektów z języka FAT.
        Znajduje nazwy obiektów i ich parametry.
        Parametry przypisywane są do self.parameters .

        self.parameters jest listą gdzie:
            -   self.parameters[0] jest listą wszystkich parametrów z nawiasów zwkłych.
            -   self.parameters[1] jest listą wszystkich parametrów z nawiasów kwadratowych.
            -   self.parameters[2] jest listą wszystkich parametrów z poza nawiasów.

        Parametry:
            type (string): Typ obiektu.
            edges (set(Edge)): Set krawędzi w programie Sgros2D.
            objects (list[FatObject]): Lista obiektów FAT powiązanych z tym obiektem.
            nameLong [string]: Nazwa obiektu długa.
            nameShort [string]: Nazwa obiektu krótka.
            parameters [list[3][string]]: parametry odczytane z FAT.
    """
    def __init__(self,line=''):
        """Kontruktor obiektu FAT. Na podstawe linii tekstu FAT,
        tworzona jest lista parametrów oraz nazwy krótka i długa.

        Args:
            line (str, optional): Linia z programu FAT. Defaults to ''.
        """
        findGroups = re.search("\+(.+?)(?=[ (])\s*(?:\(\s*(.+?)\s*\))?\s*(?:\[\s*(.+?)\s*\])?\s*(.*)",line)
        self.type = "Object"
        self.edges = set()
        self.objects = []
        if findGroups != None:
            self.nameLong = findGroups.group(1)
            self.nameShort = self.nameLong[2:]
            self.parameters = [findGroups.group(e).split(' ') if findGroups.group(e) != None else None for e in range(2,5)]

    def __str__(self):
        return self.nameLong

    def __eq__(self,other):
        """2 obiekty są sobie równe jeżeli zgadzają się ich nazwy.
            używane do przypisania obiektów
        """
        if isinstance(other,str):
            return self.nameLong == other
        return self.nameLong == other.nameLong

    def createEdges(self):
        """Tworzy krawędzie na podstawie listy przypisanych obiektów.
        Lista obiektów musi być podana w kolejności tworzenia krawędzi.
        Jeżeli obok siebie występują dwa obiekty o typie 'Point', tworzona jest z nich krawędź.
        Jeżeli aktualnie sprawdzany obiekt nie jest typu Point wywoływana jest jego funkcja createEdges().

        Returns:
            list[Edge]: Zwraca listę stworzonych krawędzi.
        """
        if len(self.edges) > 0:
            return self.edges
        for previousObject,currentObject in zip( [FatObject()]+self.objects, self.objects):
            if currentObject.type != 'Point':
                self.edges = self.edges.union( currentObject.createEdges())
            elif previousObject.type == 'Point':
                self.edges.add( Edge(previousObject, currentObject))
        return self.edges

class PointFatObject(FatObject):
    """Obiekt FAT typu punkt,
        dziedziczy po FatObject

        Parametry:
            X (float): współrzędne X punktu
            Y (float): wpsółrzędne Y punktu
    """
    def __init__(self,line='',X=None,Y=None,nameLong=None,nameShort=None):
        """Kontsruktor tworzy obiekt albo na podstawie linii z języka FAT
        lub poprzez jawne podanie współrzędnych i nazwy.

        Parametr line bierze pierwszeństwo nad jawnym podaniem współrzędnych,
        jeżeli nie podany zostanie parametr line wtedy wartośći nie podane jawnie będą miały wartość None !!

        Args:
            line (string, optional): Linia tekstu z programu FAT. Defaults to None.
            X (float, optional): Współrzędne X. Defaults to None.
            Y (float, optional): Współrzędne Y. Defaults to None.
            nameLong (string, optional): Nazwa długa. Defaults to None.
            nameShort (string, optional): Nazwa krótka. Defaults to None.
        """
        self.X = X
        self.Y = Y
        self.nameLong = nameLong
        self.nameShort = nameShort
        super(PointFatObject, self).__init__(line)
        if line != '':
            self.X,self.Y = [nC.calculateValue(self.parameters[2][e]) for e in range(0,2)]
        self.type = 'Point'

    def __str__(self):
        return 'Point X: ' + str(self.X) + ' Y: ' + str(self.Y) 
        
    def __ne__(self,other):
        return not self.__eq__(other)

    def __eq__(self,other):
        """Punkty są równe jeżeli ich wpsółrzędne są równe
        """
        if other == None:
            return False
        return self.X == other.X and self.Y == other.Y

    def __sub__(self,other):
        return self.__add__(-other)

    def __add__(self,other):
        """Suma punktów to punkt o:
             - X równym (sumie współrzędnych X dwóch punktów)
             - Y równym (sumie współrzędnych Y dwóch punktów)

        Args:
            other (list or PointFatObject): Funkcja może przyjąć jako parametr PointFatObject lub dwu elementową listę.

        Returns:
            PointFatObject: Zwracany jest stworzony punkt.
        """
        if isinstance(other,list):
            return PointFatObject(X=self.X + other[0], Y=self.Y + other[1])
        return PointFatObject(X=self.X + other.X, Y=self.Y + other.Y)

    def __neg__(self):
        return self.__mul__(-1)

    def __mul__(self,other):
        """Mnożenie współrzędnych przez liczbę.
        Funkcja zwraca nowy punkt którego współrzędne są pomnożone przez stała podaną w argumencie other.

        Args:
            other (float): Wartość przez która mają być wymnożone współrzędne.

        Returns:
            PointFatObject: Zwracany jest stworzony punkt.
        """
        return PointFatObject(X=self.X * other, Y=self.Y * other)

    def calculateDistance(self,other):
        """Zwraca odległość między dwoma punktami

        Returns:
            float: Odległość między punktami.
        """
        return decimal.Decimal(math.sqrt( (self.X - other.X)**2 + (self.Y - other.Y)**2 ))

    def calculateAngleFromVertex(self,p1,p2):
        """wylicza kąt między trzema punktami,
            wierzchołek jest punktem z którego wywoływana jest funkcja.
            Wartość kąta wyliczana jest z twierdzenia cosinusów.

                p1--> \\          //<-- p2
                       \\        //
                        \\------//
                         \\alfa//
                          \\  //
                           \\// <-- self

        Args:
            p1 (PointFatObject): Pierwszy punkt.
            p2 (PointFatObject): Drugi punkt.

        Returns:
            float: Wartość kąta wyrażona w stopniach.
        """
        a = p1.calculateDistance(self)
        b = p2.calculateDistance(self)
        c = p1.calculateDistance(p2)
        return math.degrees( math.acos( (a**2+b**2-c**2)/(2*a*b)) )

    def getClosest(self,points):
        """Funkcja wyszukuje nabliższy punkt z listy argumentu points.

        Args:
            points (list[PointFatObject]): Lista punktów.

        Returns:
            PointFatObject: Najblizszy punkt z listy punktów.
        """
        closest = points[0]
        for point in points:
            if self.calculateDistance(point) <  self.calculateDistance(closest):
                closest = point
        return closest

class MaterialFatObject(FatObject):
    """Obiekt przechowujący materiał z programu FAT
        generowany jest z linii programu FAT

    Parametry:
        Value (float): Wartość liczbowa materiału.
    """
    def __init__(self,line):
        super(MaterialFatObject, self).__init__(line)
        self.type = 'Material'
        self.Value = nC.calculateValue(self.parameters[1][0])

class CircleFatObject(PointFatObject):
    """Obiekt okręgu z programu FAT
        obiekt generowant jest z linii programu FAT

    Parametry:
        radius (float): Średnica okręgu.
        quadrants (list[PointFAtObject]): Lista punktów które odpowiadają 4 punktom okręu na granicy ćwiartek okręgu.
    """
    def __init__(self,line):
        super(CircleFatObject, self).__init__(line)
        self.type = 'Circle'
        self.radius = nC.calculateValue(self.parameters[2][2]) 
        self.quadrants= [self+e for e in [ [0,self.radius], [self.radius,0], [0,-self.radius], [-self.radius,0]]]    

    def getCommonPointsWithLine(self,m,c):
        """Zwraca wspólne punkty okręgu z prostą opisaną równaniem:
            Y = mx+ c

        Args:
            m (float): Wartość parametru równania prostej opisujący jej nachylenie.
            c (float): Wartość parametru c.

        Returns:
            tuple[PointFatObject,PointFatObject]: Tupla z 2 punktami które przecinaja okrąg.
        """
        if m == "inf":
            delta = (-(self.X**2) +2*self.X*c + self.radius**2 - c**2).sqrt()
            return PointFatObject(X= c, Y= delta+ self.Y), PointFatObject(X= c, Y= -delta+ self.Y)
        delta = (-self.X**2*m**2 + 2*self.Y*(self.X*m + c) - 2*self.X*c*m - self.Y**2 - c**2 + m**2*self.radius**2 + self.radius**2).sqrt()

        xCurve0 = (delta + self.X + self.Y*m - c*m)/(m**2 + 1)
        yCurve0 = m*xCurve0+c

        xCurve1 = (-delta + self.X + self.Y*m - c*m)/(m**2 + 1)
        yCurve1 = m*xCurve1+c
        return PointFatObject(X=xCurve0,Y=yCurve0),PointFatObject(X=xCurve1,Y=yCurve1)

class SegmentFatObject(FatObject):
    """Obiekt opisujący segment z programu FAT
        generowany jest z linii programu FAT

    parametry:
        objects (list[FatObject]): Lista obiektów należących do segmentu.
        circle (CircleFatObject): Obiekt okręgu przypisany do segmentu.
    """
    def __init__(self,line):
        super(SegmentFatObject, self).__init__(line)
        self.objects = [self.parameters[2][0],self.parameters[2][1]]
        self.type = 'Segment'
        
        if len(self.parameters[2]) == 3:
            if self.parameters[2][2][0] == '-':
                self.type = 'CircleSegment'
                self.circle = 'cr' + self.parameters[2][2][1:]

    def checkQuadrant(self,point):
        """Sprawdza w której ćwiartce znajduje się punkt

        Args:
            point (PointFatObject): Punkt który jest sprawdzany.

        Returns:
            int: Numer ćwiartki w której znajduje się punkt.
        """
        point = point - self.circle
        if point.X>=0:
            if point.Y>0:
                return 0
            if point.Y<=0:
                if point.X==0:
                    return 2
                return 1
        else:
            if point.Y>=0:
                return 3
            else:
                return 2

    def createEdges(self):
        """Jeżeli segment jest częścią okręgu wykorzystuje własną funkcję do stworzenia krawędzi
        w przeciwnym wypadku korzysta z funkcji rodzica

        Returns:
            list[Edges]: Lista krawędzi.
        """
        if self.type == "CircleSegment":
            return self.createCircleEdges()
        else:
            return super(SegmentFatObject,self).createEdges()
    
    def createCircleEdges(self):
        """Tworzy krawędzie na podstawie 2 punktów znajdujących się w parametrze self.objects
        tak aby krawędzie tworzyły fragment okręgu pomiędzy tymi punktami.


        Returns:
            list[Edges]: Lista krawędzi.
        """
        def appendEdge(p1,p2,quadrant=0):
            self.edges.add( Edge(p1,p2,circle=self.circle) )
            quadrant = quadrant + 1 if quadrant + 1 < 4 else 0
            return p2,quadrant
            
        self.edges = set()
        quadrants = [self.checkQuadrant(self.objects[0]),self.checkQuadrant(self.objects[1])]
        previousPoint = self.objects[0]
        quadrantPoints = self.circle.quadrants
        quadrantTemp = quadrants[0] + 1 if quadrants[0]+1<4 else 0

        if quadrants[0]==quadrants[1]:
            if quadrantPoints[quadrantTemp].getClosest([self.objects[1],self.objects[0]]) == self.objects[1]:
                appendEdge(self.objects[0],self.objects[1])
                return self.edges
            else:
                previousPoint,quadrantTemp = appendEdge(previousPoint,quadrantPoints[quadrantTemp],quadrantTemp)
        while (quadrantTemp-1 if quadrantTemp-1 >=0 else 3) != quadrants[1]:
            previousPoint,quadrantTemp = appendEdge(previousPoint,quadrantPoints[quadrantTemp],quadrantTemp)
        quadrantTemp = (quadrantTemp-1 if quadrantTemp-1 >=0 else 3)
        if self.objects[1] != quadrantPoints[quadrantTemp]:
            appendEdge(quadrantPoints[quadrantTemp],self.objects[1],quadrantTemp)
        
        return self.edges

class ChainFatObject(FatObject):
    """Obiekt Chain z pogramu FAT.
    obiekt jest tworzony z linii programu FAT

    Parametry:
        objects (list[FatObject]): Lista obiektów FAT z linii programu FAT.
    """
    def __init__(self,line):
        super(ChainFatObject, self).__init__(line)
        self.type = 'Chain'
        self.objects = self.parameters[2]

class MacroElementFatObject(FatObject):
    """Makro element z programu FAT
    tworzony jest na podstawie linii programu FAT

    Parametry:
        material (MaterialFatObject): Obiekt opisujący materiał makro elementu.
        sourceFunction (list[float]): Lista wartości opisujących wartości funkcji.
        raster (float): Wartość opisująca wartość rastra.
        weight (float): Wartość opisująca "wagę" obiektu.
        objects (list[FatObjects]): Lista zawierająca obiekty FAT tworzące makro element.
    """
    def __init__(self,line):
        super(MacroElementFatObject, self).__init__(line)
        self.type = 'MacroElement'
        self.material = self.parameters[0][0]
        i = len(self.parameters[0]) - 2
        self.sourceFunction = [nC.calculateValue(self.parameters[0][e]) for e in range(1,i)]
        self.raster = nC.calculateValue(self.parameters[0][i])
        self.weight = nC.calculateValue(self.parameters[0][i+1])
        self.objects = self.parameters[2]

    def znajdzEtykiete(self):
        """Znajduje współrzędne punktu w którym ma być dodana etykieta w programie Agros 2D
        etykieta potrzebna jest do określenia materiału makroelementu


        W celu znalezienia pozycji etykiety najpierw wybierana jest krawędź z krawędzi makro elementu.
        Następnie sprawdzane jest czy jeden z 4 punktów leżących na prostej pionowej i poziomej leży wewnątrz figury.
        Jeżeli tak zwraca ten punkt. Jeżeli nie odległość między punktem a krawędzią jest zmniejszana i proces jest powtarzany.

        Returns:
            PointFatObject: Punkt w którym ma znajdować się etykieta makro elementu.
        """
        def CzyLiniaPoziomaPrzecinaKrawedz(p,p0,p1):
            if p0.Y > p1.Y:
                p0, p1 = p1, p0
            if p1.Y==p0.Y:
                return 0
            if p0.Y < p.Y and p1.Y <= p.Y:
                return 0
            if p0.Y > p.Y and p1.Y > p.Y:
                return 0
            if p0.X<p.X and p1.X < p.X:
                return 0
            if p0.X >= p.X and p1.X >= p.X:
                return 1
            t  = (p.Y-p0.Y)/decimal.Decimal(p1.Y-p0.Y)
            xp = p0.X + t*(p1.X-p0.X)
            if xp >= p.X:
                return 1
            else:
                return 0

        startSzukania = next(iter(self.edges)).middleOfCurve
        for divider in [1/decimal.Decimal(10**math.ceil(x/2)*((x%-2)*2+1)) for x in range(5,40)]:
            for e in [startSzukania + [0,divider], startSzukania + [divider,0]]:
                suma = 0
                for edge in self.edges:
                    suma += CzyLiniaPoziomaPrzecinaKrawedz(e, edge.p1, edge.middleOfCurve)
                    suma += CzyLiniaPoziomaPrzecinaKrawedz(e, edge.middleOfCurve, edge.p2)
                if suma % 2 == 1:
                    validPoint = True
                    for edge in self.edges:
                        if edge.pointOnEdge(e):
                            validPoint = False
                    if validPoint:
                        return e

class BoundaryCondition():
    """Warunki brzegowe z programu FAT.
    Tworzone są na podstawie linii tekstu programu FAT.

    Parametry:
        objects (list[FatObject]): Lista obiektów których dotyczy warunek brzegowy.
        method (string): Jaki warunek brzegowy ma byc zastosowany.
        value (float): Wartość liczbowa warunku brzegowego.
    """
    def __init__(self,line):
        separateLine = line.strip().split()
        self.objects = [separateLine[1]]
        self.method = separateLine[2]
        self.value = nC.calculateValue(separateLine[3])

def findObjectInList(name,objectList):
    """Zwraca obiekt z listy obiektów o podanej nazwie.


    Args:
        name (string): Nazwa obiektu.
        objectList (list[FatObject]): Lista obiektów FAtObjects.

    Returns:
        FatObject: Obiekt o nazwie argumentu name.
    """
    for e in objectList:
        if e.nameLong == name:
            return e
    for e in objectList:
        if e.nameShort == name:
            return e

class FATLoader():
    """Obiekt ładujący dane z pliku programu FAT do obiektów w pythonie.

    parametry:
        boundaryConditions list[BoundaryCondition]: Lista warunków brzegowych.
        materials list[Material]: Lista materiałów.
        edges (list[Edge]): Lista krawędzi w programie
        etykiety (list[PointFatObjects,MacroFatObject]): Lista punktów w którym mają znajdować się etykiety oraz odpowiadające im obiekty makro.
        nazwaProgramu (string): Nazwa programu podana w pliku programu FAT.
        equation (string): Typ pola np. magnetic lub electrostatic.
        mat (list[string]): Lista nazwa rodzajów materiałów w programie agros 2D.
    """
    def __init__(self,plik):
        self.boundaryConditions = []
        self.materials = []
        basePoints = []
        circles = []
        segments = []
        chains = []
        macroElements = []

        komendy = {"problem": self.problem,
                    "eqtn":self.eqtn,
                    "bnd":lambda line: self.boundaryConditions.append(BoundaryCondition(line)),
                    "+mt":lambda line: self.materials.append(MaterialFatObject(line)),
                    "+bp":lambda line: basePoints.append(PointFatObject(line)),
                    "+cr":lambda line: circles.append(CircleFatObject(line)),
                    "+sg":lambda line: segments.append(SegmentFatObject(line)),
                    "+ch":lambda line: chains.append(ChainFatObject(line)),
                    "+me":lambda line: macroElements.append(MacroElementFatObject(line)),
                    }

        with open(plik,'r') as plik:
            plik = plik.read()
            FileLines = [re.sub(" +"," ",e) for e in re.sub('"[\s\S]+?"',"",plik[plik.find("problem"):]).split("\n") if e != '']

        for e in FileLines:
            if e[0] == "$":
                e=e[1:].strip().split(' ')
                nC.stale[e[0]] = nC.calculateValue(e[1])
            else:
                try:
                    komendy[re.search('(\+.{2}|.+?(?:[ (])).*',e).group(1).strip()](e)
                except:
                    pass

        allObjects = basePoints + circles + segments + chains + macroElements

        for e in macroElements+chains+segments+self.boundaryConditions:
            e.objects = [findObjectInList(z,allObjects) for z in e.objects]

        for e in segments:
            if e.type == 'CircleSegment':
                e.circle = findObjectInList(e.circle,allObjects)

        for material in self.materials:
            if self.equation == 'electrostatic':
                material.Value = nC.calculateValue(str(material.Value)+'/eps0') 
            if self.equation == 'magnetic':
                material.Value = nC.calculateValue(str(material.Value)+'*mu_0') 
                
        for e in macroElements:
            e.material = findObjectInList(e.material,self.materials)

        for e in segments+chains+macroElements:
            e.createEdges()

        self.edges = set()
        
        self.etykiety = []
        
        for macro in macroElements:
            for e in macro.edges:
                self.edges.add(e)
            self.etykiety.append([macro.znajdzEtykiete(),macro])
    def problem(self,linia):
        """Odczytuje nazwę programu z pierwszej linii i wpisują ją do parametru nazwaProgramu

        Args:
            linia (string): Pierwsza linia z pliku programu FAT.
        """
        self.nazwaProgramu = re.search("\'(.+)\'",linia).group(1)

    def eqtn(self,linia):        
        """Odczytuje w jakim polu ma być analizowany obiekt i wpisuje odpowiednie wartości do zmiennej
        equation i mat.

        Args:
            linia (string): Linia z pliku programu FAT.
        """
        self.equation = eqtTypes[linia.split(" ")[1]]

        self.mat    = materialTypes[linia.split(" ")[1]]