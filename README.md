Autor: Szymon Szlachtowicz

Program na magisterkę 

Interpreter języka FAT do programu Agros 2D

## Struktura Folderów w repozytorium

│   .gitignore             - Lista plików ignorowanych przez GIT  
│   agrosImporter.py            - Moduł odpowiedzialny za wczytanie obiektu LoadFile do Agrosa  
│   FAT2Agros.py                - Główny moduł odpowiedzialny za wczytywanie FAT do Agros2D  
│   loadFile.py                 - Moduł pythona wczytujący pliki FAT  
│   numberCalculations.py       - Moduł pythona zawierający funkcje obliczające wyrażenia  
│   README.md                   - Plik opisujący użycie interpretera  
│   __init__.py                 - Plik który określa folder jako moduł pythona  
│  
├───docs                        - Folder zawierający dokumentację programu  
├───examples                    - Folder zawierający przykładowe programy FAT  
└───tests                       - Folder zawierający testy jednostkowe  

## Jak używać

Są 2 sposoby na używanie interpretera FAT.

1) Użyć programu jako skryptu

W pliku FAT2Agros.py
w 11 linii należy zmienić ścieżkę na ścieżkę do pliku z programem FAT
```main('/prg.fat')```
A następnie uruchomić skrypt poprzez program Agros2D

2) Zaimportować moduł
Należy w skopiować folder InterpreterFATMagisterka 
do folderu `/Lib/site-packages` w folderze instalacji programu Agros2D
np:
`/home/Agros2D/Lib/site-packages` w przypadku Linuxa,
`C:\Program Files (x86)\Agros2D\Lib\site-packages` w przypadku Windowsa.

po skopiowaniu folderu możliwy jest import modułu FAT2Agros za pomocą linii `from InterpreterFATMagisterka.FAT2Agros import main` w pythonLab w Agros2D.
Następnie wystarczy wywołać `main("/home/Agros2D/Lib/site-packages/InterpreterFATMagisterka/examples/prg.fat")` gdzie w cudzysłowiu należy umieścić ścieżkę do pliku FAT.
Ścieżka musi być absolutna.

## Dokumentacja

Dokumentacja znajduje się w folderze docs
Opisane są tam klasy w modułach LoadFile, NumbersCalculations, AgrosImporter.
plik index.html służy za pomoc w poruszaniu się między dokumentacjami.
Dokumentacja wygenerowana została z Docstrings każdej klasy i funkcji.