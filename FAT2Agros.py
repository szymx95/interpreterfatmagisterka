import loadFile

import agrosImporter

def main(FILE_LINK):
    loader = loadFile.FATLoader(FILE_LINK)
    agrosImporter.importToAgros(loader)
    
    
if __name__ == '__main__':
    main('examples/prg.fat')    