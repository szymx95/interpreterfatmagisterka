import unittest
import sys
sys.path.append('..')

import decimal
import loadFile
import os

class TestLoadFileTestCase(unittest.TestCase):
    def test_loadFile_loads_correct_file_and_reads_it_properly(self):
        filePath = os.path.dirname(__file__)


        fatprg = loadFile.FATLoader(filePath + "/testFiles/prg.fat")
        self.assertEqual(len(fatprg.boundaryConditions),2)
        self.assertEqual(len(fatprg.materials),1)
   
        self.assertEqual(len(fatprg.edges),5)

    def test_LoadFile_BoundaryCondition_creates_object_correctly(self):
        testBoundary = loadFile.BoundaryCondition('bnd zyla diri 10')
        self.assertEqual(testBoundary.objects,['zyla'])
        self.assertEqual(testBoundary.method,'diri')
        self.assertEqual(testBoundary.value,10)

        testBoundary = loadFile.BoundaryCondition('bnd pancerz diri 0')
        self.assertEqual(testBoundary.objects,['pancerz'])
        self.assertEqual(testBoundary.method,'diri')
        self.assertEqual(testBoundary.value,0)

    def test_LoadFile_FatObject_correctly_creates_object(self):
        testObject = loadFile.FatObject('+bp1 0.1 5m')
        testObject2 = loadFile.FatObject('+bp1 0.1 5m')
        testObject3 = loadFile.FatObject('+bp2 0.2 5m')
        self.assertEqual(testObject.nameLong,'bp1')
        self.assertEqual(testObject.nameShort,'1')

        self.assertEqual(testObject,testObject2)
        self.assertNotEqual(testObject,testObject3)
        self.assertEqual(str(testObject),'bp1')
        self.assertEqual(testObject,'bp1')

        testObject = loadFile.FatObject('+chzyla bp1 bp4 bp3')
        self.assertEqual(testObject.nameLong,'chzyla')
        self.assertEqual(testObject.nameShort,'zyla')


        testObject = loadFile.FatObject('+mt1 [5*eps0]')
        self.assertEqual(testObject.nameLong,'mt1')
        self.assertEqual(testObject.nameShort,'1')


        testObject = loadFile.FatObject('+mediel( mt1 0 .25m 0 ) zyla bp1 bp2 pancerz bp5 bp3')
        self.assertEqual(testObject.nameLong,'mediel')
        self.assertEqual(testObject.nameShort,'diel')


    def test_LoadFile_PointFatObject_correctly_creates_point(self):
        testPoint = loadFile.PointFatObject('+bp1 0.1 5m')
        self.assertEqual(testPoint.type,'Point')
        self.assertEqual(testPoint.X,decimal.Decimal('0.1'))
        self.assertEqual(testPoint.Y,decimal.Decimal('0.005'))
        self.assertEqual(str(testPoint),'Point X: 0.1 Y: 0.005')

        testPoint = loadFile.PointFatObject('+bp1 -0.1 5m')
        self.assertEqual(testPoint.type,'Point')
        self.assertEqual(testPoint.X,-1*decimal.Decimal('0.1'))
        self.assertEqual(testPoint.Y,decimal.Decimal('0.005'))
        self.assertEqual(str(testPoint),'Point X: -0.1 Y: 0.005')

        testPoint = loadFile.PointFatObject('+bp1 0.1 -5m')
        self.assertEqual(testPoint.type,'Point')
        self.assertEqual(testPoint.X,decimal.Decimal('0.1'))
        self.assertEqual(testPoint.Y,-1*decimal.Decimal('0.005'))
        self.assertEqual(str(testPoint),'Point X: 0.1 Y: -0.005')

        testPoint = loadFile.PointFatObject('+bp1 2m -1m')
        self.assertEqual(testPoint.type,'Point')
        self.assertEqual(testPoint.X,decimal.Decimal('0.002'))
        self.assertEqual(testPoint.Y,-1*decimal.Decimal('0.001'))
        self.assertEqual(str(testPoint),'Point X: 0.002 Y: -0.001')

    def test_LoadFile_MaterialFatObject_correctly_creates_material(self):
        testMaterial = loadFile.MaterialFatObject('+mt1 [5*eps0]')
        self.assertEqual(testMaterial.type,'Material')
        self.assertEqual(testMaterial.Value,decimal.Decimal('8.85418781762039e-12')*5)

    def test_LoadFile_CircleFatObject_correctly_creates_circle(self):
        testCircle = loadFile.CircleFatObject('+cr1 1 2+1*2 4.5m')
        self.assertEqual(testCircle.type,'Circle')
        self.assertEqual(testCircle.X,1)
        self.assertEqual(testCircle.Y,6)
        self.assertEqual(testCircle.radius,decimal.Decimal('0.0045'))

    def test_LoadFile_SegmentFatObject_correctly_creates_segment(self):
        testSegment = loadFile.SegmentFatObject('+sg1 bp1 bp2 -1')
        self.assertEqual(testSegment.type,'CircleSegment')
        self.assertEqual(testSegment.objects,['bp1','bp2'])
        self.assertEqual(testSegment.circle,'cr1')

        testSegment = loadFile.SegmentFatObject('+sg1 bp2 bp3')
        self.assertEqual(testSegment.type,'Segment')
        self.assertEqual(testSegment.objects,['bp2','bp3'])


    def test_LoadFile_ChainFatObject_correctly_creates_chains(self):
        testChain = loadFile.ChainFatObject('+chzyla bp1 bp4 bp3')
        self.assertEqual(testChain.type,'Chain')
        self.assertEqual(testChain.objects,['bp1','bp4','bp3'])

        testChain = loadFile.ChainFatObject('+chpancerz sg1')
        self.assertEqual(testChain.type,'Chain')
        self.assertEqual(testChain.objects,['sg1']) 

    def test_LoadFile_MacroElementFatObject_correctly_creates_macro_element(self):
        testMacroElement = loadFile.MacroElementFatObject('+mediel( mt1 0 .25m 0 ) zyla bp1 bp2 pancerz bp5 bp3')
        self.assertEqual(testMacroElement.type,'MacroElement')
        self.assertEqual(testMacroElement.material,'mt1')
        self.assertEqual(testMacroElement.sourceFunction,[0])
        self.assertEqual(testMacroElement.raster,decimal.Decimal('0.00025'))
        self.assertEqual(testMacroElement.weight,0)
        self.assertEqual(testMacroElement.objects,['zyla' ,'bp1' ,'bp2' ,'pancerz' ,'bp5', 'bp3'])


        testMacroElement = loadFile.MacroElementFatObject('+mediel( mt1 0 1+2 2.5m 0 ) zyla bp1 bp2 pancerz bp5 bp3')
        self.assertEqual(testMacroElement.type,'MacroElement')
        self.assertEqual(testMacroElement.material,'mt1')
        self.assertEqual(testMacroElement.sourceFunction,[0,3])
        self.assertEqual(testMacroElement.raster,decimal.Decimal('0.0025'))
        self.assertEqual(testMacroElement.weight,0)
        self.assertEqual(testMacroElement.objects,['zyla' ,'bp1' ,'bp2' ,'pancerz' ,'bp5', 'bp3'])

unittest.main()