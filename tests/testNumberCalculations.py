import sys
sys.path.append('..')

import numberCalculations
import unittest
import decimal

class TestNumberCalculationsTestCase(unittest.TestCase):
    
    def test_numberCalculations_calculates_value_correctly(self):

        self.assertEqual(numberCalculations.calculateValue('12'),decimal.Decimal(12))
        self.assertEqual(numberCalculations.calculateValue('12e2'),decimal.Decimal(1200))
        self.assertEqual(numberCalculations.calculateValue('12e-2'),decimal.Decimal('0.12'))

        self.assertEqual(numberCalculations.calculateValue('12e-2+2'),decimal.Decimal('2.12'))
        self.assertEqual(numberCalculations.calculateValue('12e-2*2'),decimal.Decimal('0.24'))

        self.assertEqual(numberCalculations.calculateValue('12E-2+2'),decimal.Decimal('2.12'))
        self.assertEqual(numberCalculations.calculateValue('12E-2*2'),decimal.Decimal('0.24'))

        self.assertEqual(numberCalculations.calculateValue('12m'),decimal.Decimal('0.012'))
        self.assertEqual(numberCalculations.calculateValue('-12'),decimal.Decimal(-12))
        self.assertEqual(numberCalculations.calculateValue('-12e2'),decimal.Decimal(-1200))
        self.assertEqual(numberCalculations.calculateValue('-12e-2'),decimal.Decimal('-0.12'))
        self.assertEqual(numberCalculations.calculateValue('2.2e-16'),decimal.Decimal('0.00000000000000022'))
        self.assertEqual(numberCalculations.calculateValue('-12m'),decimal.Decimal('-0.012'))

        self.assertEqual(numberCalculations.calculateValue('-eps0'),-1*decimal.Decimal('8.85418781762039e-12'))
        self.assertEqual(numberCalculations.calculateValue('eps0'),decimal.Decimal('8.85418781762039e-12'))
        self.assertEqual(numberCalculations.calculateValue('12'),12)
        self.assertEqual(numberCalculations.calculateValue('-12'),-12)
        self.assertEqual(numberCalculations.calculateValue('1.5+2'),3.5)
        self.assertEqual(numberCalculations.calculateValue('-1+2'),1)
        self.assertEqual(numberCalculations.calculateValue('4/2+1*3'),9)
        self.assertEqual(numberCalculations.calculateValue('4/2+1*3-5*3'),12)
        self.assertEqual(numberCalculations.calculateValue('-4/2+1*3-5*3'),-24)
        self.assertEqual(numberCalculations.calculateValue('eps0+3'),decimal.Decimal('8.85418781762039e-12')+3)
        self.assertEqual(numberCalculations.calculateValue('3*eps0'),decimal.Decimal('8.85418781762039e-12')*3)
        self.assertEqual(numberCalculations.calculateValue('eps0*3'),decimal.Decimal('8.85418781762039e-12')*3)
        self.assertEqual(numberCalculations.calculateValue('-eps0+3'),-1*decimal.Decimal('8.85418781762039e-12')+3)

    def test_numberCalculations_getValueOfNumber_returns_correct_number(self):
        self.assertEqual(numberCalculations.getValueOfNumber('12'),decimal.Decimal(12))
        self.assertEqual(numberCalculations.getValueOfNumber('12e2'),decimal.Decimal(1200))
        self.assertEqual(numberCalculations.getValueOfNumber('12e-2'),decimal.Decimal('0.12'))
        self.assertEqual(numberCalculations.getValueOfNumber('12m'),decimal.Decimal('0.012'))
        self.assertEqual(numberCalculations.getValueOfNumber('-12'),decimal.Decimal(-12))
        self.assertEqual(numberCalculations.getValueOfNumber('-12e2'),decimal.Decimal(-1200))
        self.assertEqual(numberCalculations.getValueOfNumber('-12e-2'),decimal.Decimal('-0.12'))
        self.assertEqual(numberCalculations.getValueOfNumber('-12m'),decimal.Decimal('-0.012'))

        self.assertEqual(numberCalculations.getValueOfNumber("eps0"),decimal.Decimal('8.85418781762039e-12'))

unittest.main()