# -*- coding: utf-8 -*-
import agros2d as a2d

boundaryTypes = {'diri':{'magnetic':['magnetic_potential','magnetic_potential_real'],'electrostatic':['electrostatic_potential','electrostatic_potential']}}

def importToAgros(loader,clear = True):
    """Funkcja importująca obiekty z klasy FATLoader do programu Agros2D.

    Args:
        loader (loadFile.FATLoader): Obiekt klasy FATLoader z załadowanym plikiem.
        clear (boolean): Czy usunąć aktualny model] w programie Agros2D.
    """
    # problem
    problem = a2d.problem(clear = clear)
    problem.coordinate_type = "planar"
    problem.mesh_type = "triangle"

    solverType = a2d.field(loader.equation)


    agrosBoundaries = []
    for counter,e in enumerate(loader.boundaryConditions):
        
        nazwa = None
        for bound in agrosBoundaries:
            if bound[1] == e.value:
                nazwa = bound[0]
        if nazwa == None:
            e.nazwa = "Warunek"+str(counter)        
            nazwa = e.nazwa
            solverType.add_boundary(e.nazwa,boundaryTypes[e.method][loader.equation][0],{boundaryTypes[e.method][loader.equation][1]:e.value})
            agrosBoundaries.append([nazwa,e.value])
        for edge in loader.edges:
            for edge2 in e.objects[0].edges:            
                if edge2 == edge:
                    edge.boundary = nazwa
            
    geometry = a2d.geometry
    for e in loader.edges:
        e.angle= round(e.angle)
        if e.boundary != '':
            geometry.add_edge(e.p1.X, e.p1.Y, e.p2.X, e.p2.Y, angle = e.angle, boundaries = {loader.equation:e.boundary})
        else:
            geometry.add_edge(e.p1.X, e.p1.Y, e.p2.X, e.p2.Y, angle = e.angle)
    agrosMaterials = []
    for i,e in enumerate(loader.etykiety):
        materialName = None
        for mat in agrosMaterials:
            if mat[1] == e[1].material.Value and mat[2] == e[1].sourceFunction[0]:
                materialName = mat[0]
        if materialName == None:
            materialName = "material"+str(len(agrosMaterials))
            solverType.add_material(materialName,{loader.mat[0]:e[1].material.Value,loader.mat[1]:e[1].sourceFunction[0]})
            agrosMaterials.append([materialName,e[1].material.Value,e[1].sourceFunction])
        geometry.add_label(e[0].X, e[0].Y, materials = { loader.equation: materialName})
    a2d.view.zoom_best_fit()