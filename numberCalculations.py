# -*- coding: utf-8 -*-
import re
import decimal


stale={"eps0":decimal.Decimal('8.85418781762039e-12'),"mi0inv":decimal.Decimal('49484484.233994376376601991971763'),'mu_0':decimal.Decimal('12.57e-7')}
SIsufixes = {"":1,"m":decimal.Decimal('0.001')}
Dzialania = {"*":lambda x,y:x*y,"/":lambda x,y:x/y,"+":lambda x,y:x+y,"-":lambda x,y:x-y}
    
def numberBetween(n1,n2,n3):
    """Funkcja zwraca czy liczba n1 zawiera się pomiędzy liczbami n2 i n3

    Args:
        n1 (float): Sprawdzana liczba.
        n2 (float): Granica.
        n3 (float): Granica.

    Returns:
        boolean: True jeżeli n1 jest pomiędzy n2 a n3, False w przeciwnym wypadku.
    """
    return min(n2,n3) <= n1 <= max(n2,n3)

def getValueOfNumber(zmienna):
    """Funkcja przyjmuje zmienną jako string i zamienia ją na typ decimal.Decimal.
        najpierw sprawdza czy zmienna znajduje się w stałych, jeżeli nie to zmienia
        zmienną w liczbę a następnie oblicza jej wartość biorąc pod uwagę przyrostek.

    Args:
        zmienna (string): Zmienna do zamiany w liczbę.

    Returns:
        decimal.Decimal: Wartość liczbowa zmiennej.
    """
    if zmienna in stale:
        return stale[zmienna]
    liczba = re.search("^ *([-\d.]+)",zmienna)
    if liczba != None:
        SI = zmienna[liczba.end():]
        liczba = decimal.Decimal(liczba.group(1))
        if SI in SIsufixes:
            return liczba*SIsufixes[SI]
        if SI.startswith('e'):
            return liczba*10**decimal.Decimal(SI[1:])
        return liczba

def calculateValue(equation):
    """Oblicza wartość równania.
    np. 5+10-2 zwraca 13

    Args:
        equation (string): Równanie do obliczenia.

    Returns:
        decimal.Decimal: Wartość równania.
    """
    equation = equation.lower()
    if len(equation) == 0:
        return None
    poprzedniaWartosc = decimal.Decimal('-1' if equation[0] == '-' else '1')
    poprzednieDzialanie = '*'
    aktualnaWartosc = ''

    for e in equation if equation[0] != '-' else equation[1:]:
        if e in Dzialania and aktualnaWartosc[-1] != 'e':
            poprzedniaWartosc = Dzialania[poprzednieDzialanie](poprzedniaWartosc, getValueOfNumber(aktualnaWartosc))
            poprzednieDzialanie = e 
            aktualnaWartosc = ''
        else:
            aktualnaWartosc += e
    return Dzialania[poprzednieDzialanie](poprzedniaWartosc, getValueOfNumber(aktualnaWartosc))
